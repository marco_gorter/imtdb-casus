Cypress.Commands.add("apiLoginUser", (user, password) => {
    cy.request({
        method: 'GET',
        url: 'http://localhost:8080/v1/proxy/users/'+user,
    })
    .then((response) => {
        cy.request({
            method: 'POST' ,
            url: 'http://localhost:8080/v1/proxy/tokens/',
            body: {
                "id": response.body.id, 
                "password": password, 
                "username": user           
            }
        })
        .then((resp) => {
            console.log(resp.body)
            window.localStorage.setItem('token', '"' + resp.body.access_token + '"')
            window.localStorage.setItem('userName', '"' + user + '"')
            window.localStorage.setItem('userId', response.body.id)
            window.localStorage.setItem('expirationDate', '"' + Cypress.moment().add(1, 'hours').toISOString() + '"')
        })
        cy.visit('/')
    })
})

Cypress.Commands.add("signupUser", (user, password) => {
    cy.get('#username').clear().type(user)
    cy.get('#password').clear().type(password)
    cy.get('#repeat-password').clear().type(password)
    cy.get('.login__submit').should('not.have.class', 'disabled').click()

Cypress.Commands.add("loginUser", (user, password) => {
    cy.get('#username').type(user)
    cy.get('#password').type(password)
    cy.get('.login__submit').should('include.text', 'Submit').click()
})

Cypress.Commands.add("apiGETMovie", (movieId, movieTitle) => {
    cy.request({
        method: 'GET' ,
        url: 'http://localhost:8080/v1/proxy/movies/',
        form: true
  
    })
    .then((resp) => {
        expect(resp.body).property('0').to.contain({
            title: "Once Upon a Time in the West"
          })
    })
})
