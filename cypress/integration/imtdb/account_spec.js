describe('Login', () => {
  // Create user with unique email address, based on timestamp
  var timestamp = new Date().getTime()
  var user = "user" + timestamp
  var password = "test" + timestamp

    beforeEach(function () {
      cy.visit('/')
      })

    it('Sign-up new user', () => {
      cy.visit('/signup')
      cy.signupUser(user, password)
      cy.get('.header__title > h2').should('contain.text', 'Successfull Created a user')
      cy.get('.cta__container').should('contain.text', 'Created user')
      cy.get('dl').should('contain.text', user)
      cy.url().should('include', user)
    })

    it('Sign-up existing user', () => {
      cy.visit('/signup')
      cy.signupUser(user, password)
      cy.get('.cta__container').should('contain.text', 'Signup Failed')
    })
    
    it('Login after signup', () => {
      cy.visit('/signupsuccess/' +user)
      cy.get('.cta__container').within(() => {
        cy.get('button').should('have.text', 'Login').click()
      })
      cy.loginUser(user, password);
      cy.get('a[href*="logout"]', { timeout: 30000 }).contains('Logout') // Extra timeout to compensate slower connection
    })

    it('Login', () => {
      cy.visit('/login')
      cy.loginUser(user, password);
      cy.get('a[href*="logout"]', { timeout: 30000 }).contains('Logout') // Extra timeout to compensate slower connection
    })

    it('Invalid login', () => {
      cy.visit('/login')
      cy.loginUser(user, 'password');
      cy.get('.cta__container', { timeout: 30000 }).should('contain.text', 'Login Failed') // Extra timeout to compensate slower connection
    })

    it('Check profile', () => {    
      cy.apiLoginUser(user, password)
      cy.get('a[href*="profile"]').contains('Profile').click()
      cy.get('dl').should('contain.text', user)
    })

    it('Logout', () => {    
      cy.apiLoginUser(user, password)
      cy.get('a[href*="logout"]').contains('Logout').click()
      cy.get('h3').should('contain.text', 'Coming soon')
    })
  })