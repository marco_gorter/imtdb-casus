describe('Navigation', () => {

    beforeEach(function () {
      cy.visit('/');
      })


    it('View home-screen', () => {
      cy.get('.logo').click()
      cy.get('h2')
        .contains('Movies');
    })

    it('View signup-screen', () => {
      cy.get('.header-menu').contains('Sign Up').click();
        cy.get('h2')
        .contains('Sign Up');
    })

    it('View login-screen', () => {
      cy.get('.header-menu').contains('Login').click();
        cy.get('h2')
        .contains('Login');
    })

    it('View all movies-screen', () => {
      cy.get('.cta__container').contains('List all movies').click();
        cy.get('h2')
        .contains('All Movies');
    })

    
  })