describe('Movies', () => {

  var movie = {
    "description": "Turned down the matrix for this",
    "imdb": "tt0120891",
    "title": "Wild Wild West",
    "type": "movie",
    "year": 1999
  }


    beforeEach(function () {    
      cy.apiLoginUser('testuser', 'user')
      cy.visit('/movies')
      })

    it('Search via search-bar', () => {
      cy.get('.search-movie__input').type(movie.title)
      cy.get('h3').contains(movie.title).as('displayedMovie')
      cy.get('@displayedMovie').parent().children('p').should('have.text', movie.description)
    })

    it('View movie details', () => {
      cy.get('h3').contains(movie.title).as('displayedMovie')
      cy.get('@displayedMovie').parent().children('button').click()
      cy.get('dl').should('contain.text', movie.title)
      cy.get('dl').should('contain.text', movie.type)
      cy.get('dl').should('contain.text', movie.year)
      cy.get('.movies > div').should('contain.text', movie.description)
    })


    
  })