# IMTDb-casus
Author(s): "Marco Gorter" <marcogorter@gmail.com>

# Introduction

This automation suite is created as assessment for the TestCoders. In this suite the following general features have been created:
- Account spec
- Movie spec
- Navigate spec


# Setup

## General

- Make sure you have NodeJS 12.16.2 or higher installed
- Make sure you have chrome installed
- Install Cypress:

    $ npm install cypress


- To run in default browser (Electron):

    $ npm run cy:run


- To run in Chrome browser:

    $ npm run cy:run-chrome
